# Coding Standards for IP3 Web App

# Getting Started

- Git Clone IP3-Web-APP
- Open in VS Code
- Create a Branch if not in there
    - Create a branch with:
    ```git checkout -b <feature/replace-with-name-like-this-naming-format>```

- Copy a page from one of the pages suitable for editing and edit it accordingly

# Naming Convention

### File names:

- file-name.ext (weather-visualisation.htm)

### Variable names:

- variableName

### Branch:

Feature is the type of branch, could be bugfix, hotfix, release. The feature is for each page, this will allow everyone to make changes independent of the master branch.

- <branchtype>/<branch-name>
- ```feature/earthquake-visualisation```

# Comments

- Comment on complex or block code or function description, not every line.

- Comments should be above the code or function, not in-line.
- Put space before commenting on JS and CSS code

### HTML:

- <!--Comment--!>

### JS AND CSS:

- // Comment single line
- /\* Comment multi line \*/

# Coding Standards

- Keep code simple but robust (should have IFs, not break)
- Use IDE formatter (Alt+Shift+f) before committing
- Keep code modular, don&#39;t create long blocks of code separate them into functions (not too many functions, know when is needed)
- Code should speak for itself. Less comments = less maintenance.
- Single-responsibility code. i.e function does 1 thing well. Less arguments = better function.

# Keep Code organised

- Put JS code in the JavaScript directory, accordingly for the rest (CSS)