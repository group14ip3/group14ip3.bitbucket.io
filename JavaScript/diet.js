
// Dynamically Created Buttons.
$(document).ready(function createButtons() {

    var btnWorld = $('<button/>', {
        text: "World", //set text 1 to 10
        id: "world",
        class: "eq-btns btn-block btn btn-sm btn-primary",
    });

    var btnEU = $('<button/>', {
        text: "EU", //set text 1 to 10
        id: "eu",
        class: "eq-btns btn-block btn btn-sm btn-primary",
    });

    var btnUK = $('<button/>', {
        text: "UK", //set text 1 to 10
        id: "uk",
        class: "eq-btns btn-block btn btn-sm btn-primary",
    });

 
    $("#regional-buttons").append(btnUK);
    $("#regional-buttons").append(btnEU);
    $("#regional-buttons").append(btnWorld);


});

/*
 Retrieving json data and displaying graphs whenever
 corresponding button is clicked.

 
$(document).ready(function () {

    $.ajax({
        
        url: "/Resources/diet_convert_csv.json",


        error: function () {
            alert('An error has occurred');
        },

        success: function (data) {
           
            console.log(data);
            
        }

    })
    
});*/



/*


$(document).ready(function () {
$.getJSON('/Resources/diet_convert_csv.json', function (data) {

//---------------------chart no 1

      // create a chart
  var chart = anychart.area();

  // set data
  chart.data(data);
  
  // set the container id
  chart.container("container");

  // initiate drawing the chart
  chart.draw();
  
    console.log(data);

//--------------------------chart no 2

// map data for the first series, take x from the zero area and value from the first area of data set
    var seriesData_1 = data.mapAs({'x': 1, 'value': 2});

    // map data for the second series, take x from the zero area and value from the second area of data set
    var seriesData_2 = data.mapAs({'x': 1, 'value': 3});

    // map data for the second series, take x from the zero area and value from the third area of data set
    var seriesData_3 = data.mapAs({'x': 0, 'value': 3});

    // map data for the fourth series, take x from the zero area and value from the fourth area of data set
    var seriesData_4 = data.mapAs({'x': 0, 'value': 4});

    // create bar chart
    var chart = anychart.area();

    // turn on chart animation
    chart.animation(true);

    // force chart to stack values by Y scale.
    chart.yScale().stackMode('percent');

    var crosshair = chart.crosshair();
    // turn on the crosshair
    crosshair.enabled(true)
            .yStroke(null)
            .xStroke('#fff')
            .zIndex(99);
    crosshair.yLabel().enabled(false);
    crosshair.xLabel().enabled(false);

    // set chart title text settings
    chart.title('Regional ratio of cosmetic products sales');
    chart.title().padding([0, 0, 10, 0]);

    // set yAxis labels formatting, force it to add % to values
    chart.yAxis(0).labels().format("{%Value}%");

    // helper function to setup label settings for all series
    var setupSeries = function (series, name) {
        series.name(name)
                .stroke('3 #fff 1')
                .fill(function () {
                    return this.sourceColor + ' 0.8'
                });
        series.markers().zIndex(100);
        series.hovered().stroke('3 #fff 1');
        series.hovered().markers()
                .enabled(true)
                .type('circle')
                .size(4)
                .stroke('1.5 #fff');
    };

    // temp variable to store series instance
    var series;

    // create first series with mapped data
    series = chart.area(seriesData_1);
    setupSeries(series, 'USA');

    // create second series with mapped data
    series = chart.area(seriesData_2);
    setupSeries(series, 'China');

    // create third series with mapped data
    series = chart.area(seriesData_3);
    setupSeries(series, 'EU');

    // create fourth series with mapped data
    series = chart.area(seriesData_4);
    setupSeries(series, 'Africa');

    // set interactivity and toolitp settings
    chart.interactivity().hoverMode('by-x');
    chart.tooltip().displayMode('union');

    // turn on legend
    chart.legend()
            .enabled(true)
            .fontSize(13)
            .padding([0, 0, 25, 0]);

    // set container id for the chart
    chart.container('container');

    // initiate chart drawing
    chart.draw();

})



});
*/