$.when(
    $.getJSON('https://raw.githubusercontent.com/highcharts/highcharts/master/samples/data/world-population-density.json', function (data) {

        // Prevent logarithmic errors in color calulcation
        $.each(data, function () {
                this.value = (this.value < 1 ? 1 : this.value);
            }),

            // Initiate the chart
            Highcharts.mapChart('containerx', {

                chart: {
                    map: 'custom/world'
                },

                title: {
                    text: ''
                },

                legend: {
                    title: {
                        text: 'Population Density of Countries around the World',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                        }
                    }
                },

                mapNavigation: {
                    enabled: true,
                    buttonOptions: {
                        verticalAlign: 'bottom'
                    }
                },

                tooltip: {
                    backgroundColor: 'none',
                    borderWidth: 0,
                    shadow: false,
                    useHTML: true,
                    padding: 0,
                    pointFormat: '<span class="f32"><span class="flag {point.properties.hc-key}">' +
                        '</span></span> {point.name}<br>' +
                        '<span style="font-size:30px">{point.value}/km²</span>',
                    positioner: function () {
                        return {
                            x: 0,
                            y: 250
                        };
                    }
                },

                colorAxis: {
                    min: 1,
                    max: 1000,
                    type: 'logarithmic'
                },

                series: [{
                    data: data,
                    joinBy: ['iso-a3', 'code3'],
                    name: 'Population ',
                    states: {
                        hover: {
                            color: '#a4edba'
                        }
                    }
                }]
            })
    })
).then(function () {

    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal 
    btn.onclick = function () {
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
});