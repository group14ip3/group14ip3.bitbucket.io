var map;

// Driver function, runs the rest of the code when the DOM has been built
$(document).ready(function () {
  createInputs();

  // JQuery variables 
  $showWeatherBtn = $("#showWeatherBtn");
  $inputPlaceName = $("#inputPlaceName");
  $inputLatitude = $("#inputLatitude");
  $inputLongitude = $("#inputLongitude");

  initialiseMap(1);

  // Show Weather button click event
  $showWeatherBtn.on("click", function () {
    showWeatherBtnHandler();
  });

  // Input change events
  $inputPlaceName.on("input", function () {
    enableDisableShowWeatherBtn();
  });
  $inputLatitude.on("input", function () {
    enableDisableShowWeatherBtn();
  });
  $inputLongitude.on("input", function () {
    enableDisableShowWeatherBtn();
  });


  // Auto setup weather visualisation with current location of user
  $.get("https://ipinfo.io?token=d8b19849eae3a3", function (response) {
    showVisualisation(response.city);
  }, "jsonp");

  $inputPlaceName.easyAutocomplete(getSearchData());
});


//-----------------------------Functions---------------------------


// Dynamically Created Input boxes
function createInputs() {
  var inputPlaceName = $("<input/>", {
    placeholder: "Place Name",
    type: "text",
    id: "inputPlaceName",
    class: "form-control",
    onClick: "this.select();"
  });

  var inputLatitude = $("<input/>", {
    placeholder: "Latitude",
    type: "number",
    step: "0.01",
    min: "0.00",
    max: "90.00",
    id: "inputLatitude",
    class: "form-control",
    onClick: "this.select();"
  });

  var inputLongitude = $("<input/>", {
    placeholder: "Longitude",
    type: "number",
    step: "0.01",
    min: "0.00",
    max: "180.00",
    id: "inputLongitude",
    class: "form-control",
    onClick: "this.select();"
  });
  $("#input-box-place-name").append(inputPlaceName);
  $("#input-boxes-coordinates").append(inputLatitude);
  $("#input-boxes-coordinates").append(inputLongitude);
}

// Initialises the GMap
function initialiseMap(zoom, coordinates) {
  map = new google.maps.Map(document.getElementById("gMaps"), {
    zoom: zoom,
    center: coordinates || new google.maps.LatLng(0, 0)
  });
}

// Displays the weather visualisation
function showVisualisation(cityNameOrCoords) {
  hideHelpMessageBox();

  // console.log("CityName: " + cityNameOrCoords);

  // A promise value can only accessed by wrapping in the 'then' function
  getWeatherData(cityNameOrCoords).then(function (data) {
    markMapLocation(data.location.lat, data.location.lon);
    setCurrentWeather(data);
    setWeekWeather(data);
  });
}

// Handles click event for show weather button
function showWeatherBtnHandler() {
  var isPlaceNameNotEmpty = !!$("#inputPlaceName").val();
  var isLatEmpty = !$("#inputLatitude").val();
  var isLongEmpty = !$("#inputLongitude").val();
  var isCoordsEmpty = isLatEmpty || isLongEmpty;

  if (isPlaceNameNotEmpty) {
    var placeName = $("#inputPlaceName").val();
    showVisualisation(placeName);
  } else if (!isPlaceNameNotEmpty && isLatEmpty && isLongEmpty) {
    $("#inputPlaceName").addClass("is-invalid");
    $("#help-message").text("Enter a place name or coordinates");
    showHelpMessageBox();
  } else if (isLatEmpty && !isLongEmpty) {
    $("#inputLatitude").addClass("is-invalid");
    $("#help-message").text("Enter a latitude coordinate");
    showHelpMessageBox();
  } else if (isLongEmpty && !isLatEmpty) {
    $("#inputLongitude").addClass("is-invalid");
    $("#help-message").text("Enter a longitude coordinate");
    showHelpMessageBox();

  } else if (!isCoordsEmpty) {
    var latitude = $("#inputLatitude").val();
    var longitude = $("#inputLongitude").val();
    var coords = latitude + "," + longitude;

    showVisualisation(coords);
  }
}

// Gets the data for a specified city or coordinates
function getWeatherData(cityNameOrCoords) {
  var url = "https://api.apixu.com/v1/forecast.json?key=321d2a6a08de4520909173444193003&q=" +
    cityNameOrCoords + "&days=6";
  return Promise.resolve($.ajax({
    async: false,
    url: url,
    type: "GET",
    dataType: "json",
    error: function () {
      $("#help-message").text("Error. Please enter a proper location.");
      showHelpMessageBox();
    },
  }));
}

// Sets html Content for current weather
function setCurrentWeather(data) {

  var cityName = data.location.name + ", " + data.location.country;
  var description = data.current.condition.text;
  var iconUrl = data.current.condition.icon;
  // iconUrl = iconUrl.replace("64x64", "128x128");
  var humidity = data.current.humidity + "%";
  var temperature = data.current.temp_c + "°C";
  var pressure = data.current.pressure_mb;
  var wind = data.current.wind_mph + " mph, " + getDirectionName(data.current.wind_dir);

  var content = '<h3 id="city-name" >' + cityName + '</h3>' +
    '<h4 class="mb-0"><img src="' + iconUrl + '" alt="' + cityName + '"/>' + '<span>' + temperature + '</span>' + '&nbsp;&nbsp;' + '<small>' + description + '</small>' + '</h4>' +
    '<table class="table table-borderless mb-0">' +
    '<tbody>' +
    '<tr >' +
    '<td class="weather-table-title">Wind</td >' +
    '<td>' + wind + '</td > ' +
    '</tr>' +
    '<tr>' +
    '<td  class="weather-table-title">Pressure</td>' +
    '<td >' + pressure + ' hectopascals (hPa)</td>' +
    '</tr>' +
    '<tr >' +
    '<td class="weather-table-title">Humidity</td>' +
    '<td >' + humidity + '</td>' +
    '</tr>' +
    '</tbody>' +
    '</table>' +
    '</div>';

  $("#current-weather").html(content);
}

// Sets html Content for week weather
function setWeekWeather(data) {

  var content = "";

  data.forecast.forecastday.forEach(function (element, index) {
    var date = new Date(element.date);
    // Ignore the first element (current weather)
    if (index !== 0) {
      date = date.format("ddd, mmmm dd");
      var description = element.day.condition.text;
      var iconUrl = element.day.condition.icon;
      var humidity = element.day.avghumidity + "%";
      var temperature = element.day.avgtemp_c + "°C";
      var wind = element.day.maxwind_mph + " mph";


      content += '<tr>' +
        ' <td>' + date + '<img src="' + iconUrl + '" alt="' + description + '"></td>' +
        '<td>' +
        '<p>' + '<span>' + temperature + '</span>' + " " + description + '</p > ' +
        '<p>' + wind + '</p>' +
        '</td>' +
        '</tr>';
    }
  });

  $("#table-week-data").html(content);

}

// Mark a location on the GMap with given coordinates
function markMapLocation(latitude, longitude) {
  var coordinates = new google.maps.LatLng(latitude, longitude);

  initialiseMap(8, coordinates);

  new google.maps.Marker({
    position: coordinates,
    map: map,
    animation: google.maps.Animation.DROP,
    title: $("#inputPlaceName").val()
  });
}

// Returns a full name for a given compass abbreviation   
function getDirectionName(direction) {
  var compassSectors = { N: "North (N)", NNE: "North-northeast (NNE)", NE: "Northeast (NE)", ENE: "East-northeast (ENE)", E: "East (E)", ESE: "East-southeast (ESE)", SE: "Southeast (SE)", SSE: "South-southeast (SSE)", S: "South (S)", SSW: "South-southwest (SSW)", SW: "Southwest (SW)", WSW: "West-southwest (WSW)", W: "West (W)", WNW: "West-northwest (WNW)", NW: "Northwest (NW)", NNW: "North-northwest (NNW)" };

  return compassSectors[direction];
}

// Gets the data for easyAutocomplete
function getSearchData() {
  return {
    url: function (userInput) {
      return "https://api.apixu.com/v1/search.json?key=321d2a6a08de4520909173444193003&q=" + userInput;
    },
    getValue: "name",
    minCharNumber: 3,
    list: {
      onChooseEvent: function () {
        $showWeatherBtn.prop("disabled", false);
        $showWeatherBtn.removeClass("disabled");
        $showWeatherBtn.click();
      },
      showAnimation: {
        type: "slide", //normal|slide|fade
        time: 300
      },
      hideAnimation: {
        type: "slide",
        time: 400
      },
      onHideListEvent: function () {
        var containerList = $('#inputPlaceName').next('.easy-autocomplete-container').find('ul'); if ($(containerList).children('li').length <= 0) { $(containerList).html('<li>No results found</li>').show(); }
      }
    }
  };
}

/**
 * Enables show weather button if the input empty
 * Disables show weather button if the input not empty
 */
function enableDisableShowWeatherBtn() {
  var isPlaceNameEmpty = !$("#inputPlaceName").val();
  var isLatEmpty = !$("#inputLatitude").val();
  var isLongEmpty = !$("#inputLongitude").val();
  var isCoordsEmpty = isLatEmpty && isLongEmpty;

  if (isPlaceNameEmpty && isCoordsEmpty) {
    if (!$showWeatherBtn.hasClass("disabled")) {
      $showWeatherBtn.addClass("disabled");
      $showWeatherBtn.prop("disabled", true);
    }
  } else if (!isPlaceNameEmpty || !isCoordsEmpty) {
    if ($showWeatherBtn.hasClass("disabled")) {
      $showWeatherBtn.removeClass("disabled");
      $showWeatherBtn.prop("disabled", false);
    }
  }
}

// Shows help message box
function showHelpMessageBox() {
  $("#help-message-box").removeClass("d-none");
}

// Hides help message box
function hideHelpMessageBox() {
  $("#help-message-box").addClass("d-none");

  // Remove invalid class  
  $("#inputPlaceName").removeClass("is-invalid");
  $("#inputLongitude").removeClass("is-invalid");
  $("#inputLatitude").removeClass("is-invalid");
}
