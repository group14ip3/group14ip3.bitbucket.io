// ##### GetJSON Population

// Collections of country name, gdp, GPDperCapita and data to be ued
var arrayPopCountryName = [];
var arrayPopCountryValue = [];
var GPDperCapita = [];
var arrayGdpValue = [];
var mydata = [];

var iso3countryCode = [];
var iso2countryCode = [];

$.when(

    // ##### GetJSON Population
    $.getJSON("https://api.worldbank.org/v2/country/all/indicator/SP.POP.TOTL?date=" 
        + globalVariable.year + "&format=json&per_page=264",
        
        function (popData) {
            // Populate array with name
            for (i = 0; i < popData[1].length; i++) {
                arrayPopCountryName.push(popData[1][i].country.value);
            }

            // Populate array with population
            for (i = 0; i < popData[1].length; i++) {
                arrayPopCountryValue.push(popData[1][i].value);
            }
        }),

    // Need the iso2 country code for finding flag images
    $.getJSON("https://api.worldbank.org/v2/country/?format=json&per_page=300", 
    function (countryData) {

        iso2countryCode = countryData;
        console.log(iso2countryCode);
    }),

    // ##### GetJSON GDP
    $.getJSON("https://api.worldbank.org/v2/country/all/indicator/NY.GDP.MKTP.CD?date=" + globalVariable.year +
        "&format=json&per_page=264",
        function (gdpData) {
            console.log(gdpData);
            // Populate array with gdp
            for (i = 0; i < gdpData[1].length; i++) {
                arrayGdpValue.push(Math.floor(gdpData[1][i].value));
            }
        }),
).then(function () {

    // ##### Do Calculation to work out GPDperCapita
    for (i = 0; i < arrayGdpValue.length; i++) {
        GPDperCapita.push(Math.floor(arrayGdpValue[i] / arrayPopCountryValue[i]));

    };

    // ##### Chart first 47 are not countries, 236 is amount of elements
    for (i = 46; i < arrayPopCountryName.length; i++) {
        mydata.push([arrayPopCountryName[i], arrayPopCountryValue[i], GPDperCapita[i]]);
    };


    // Pop World off as its not a country and messes with the graph
    // Pop of Eritrea as it has no value
    for (i = 0; i < mydata.length; i++) {
        for (j = 0; j < 3; j++) {
            if (mydata[i][j] === "World") {
                mydata.splice(i, 1);
            }
            if (mydata[i][j] === "Eritrea") {
                mydata.splice(i, 1);
            }
        }
    }

    //Sort array before I cut of the lowest according to selection
    mydata.sort(function (a, b) {
        return b[2] - a[2];
    });

    // Now cut of counrtries according to selection
    mydata.splice(globalVariable.countries, 
        (mydata.length - globalVariable.countries + 1));

    // create a data set
    var data = anychart.data.set(mydata);

    anychart.onDocumentReady(function () {

        // map the data
        var seriesData_1 = data.mapAs({
            x: 0,
            value: 1
        });
        var seriesData_2 = data.mapAs({
            x: 0,
            value: 2
        });

        // create a chart
        var chart = anychart.column();

        // enable the value stacking mode
        chart.yScale().stackMode("value");


        // create area series, set the data can use line, bar, column
        var series1 = chart.column(seriesData_1);
        var series2 = chart.line(seriesData_2);

        // Stroke colour and thickness
        series2.stroke("5 orange 5");
        series2.hovered().stroke("5 red 5");

        // Name data
        series1.name("Population");
        series2.name("GDP per Capita");

        // configure tooltips
        chart.tooltip().fontColor("gold");


        // configure labels on the y-axis
        var yAxis1 = chart.yAxis(0);
        var yScale1 = anychart.scales.linear();

        // Set left Scale
        yAxis1.scale(yScale1);
        yAxis1.title("Population");

        var yScale2 = anychart.scales.linear();

        // Set the right hand side scale
        var yAxis2 = chart.yAxis(1);
        yAxis2.orientation("right")
        yAxis2.scale(yScale2);
        yAxis2.title("GDP per Capita");

        chart.yAxis(1).labels().format("${%value}");

        // Formats lables x & y with letters to save some space of large numbers
        var yTicks2 = chart.yScale().ticks();
        yTicks2.interval(20000000);
        var yLabels2 = chart.yAxis(1).labels();
        yLabels2.format("${%value}{scale: (1)(1000)(1000)(1000)|(k)(K)(M)(B)}");
        var yTicks = chart.yScale().ticks();
        yTicks.interval(20000000);
        var yLabels = chart.yAxis().labels();
        yLabels.format("{%value}{scale: (1)(1000)(1000)(1000)|(k)(m)(M)(B)}");

        // Rotates x lables and places them inside chart
        chart.xAxis().labels().rotation(45);
        chart.xAxis().labels().position("inside");

        series1.yScale(yScale1);

        series2.yScale(yScale2);

        var axisLabels = chart.xAxis().labels();
        axisLabels.useHtml(true);
        axisLabels.format("<b style='text-orientation:upright;'>{%value}</b>");
        axisLabels.fontColor("#000000");

        // set the chart title
        chart.title("Year: " + globalVariable.year + "\nTop: " + globalVariable.countries + " countries by GDP per Capita");

        var title = chart.title();
        title.fontColor("black");
        title.fontDecoration("bold");

        // Display the legend at the top of the chart
        chart.legend(true);

        // adjust markers of a series
        var markers = series2.markers();
        markers.enabled(true);
        markers.fill("#BBDA00");
        markers.stroke("2 white");
        markers.size(10);
        var hoverMarkers = series2.hovered().markers();
        hoverMarkers.fill("darkred");
        hoverMarkers.stroke("2 white");
        hoverMarkers.size(15);

        // set the container id
        chart.container("anyCgraph");

        /* Opens wikipedia page of element double clicked on */
        // add a listener
        chart.listen("pointDblClick", function (e) {
            var index = e.point.getIndex();
            var country = mydata[index]
            window.open("https://en.wikipedia.org/wiki/" + country[0]);
        });

        chart.listen("pointMouseOver", function (e) {
            var index = e.point.getIndex();
            var country = mydata[index]
            var countryCode;

            for (i = 0; i < iso2countryCode[1].length; i++) {
                if (country[0] === iso2countryCode[1][i].name) {
                    countryCode = iso2countryCode[1][i].iso2Code;

                    console.log(countryCode);
                    var flagImage = "https://www.countryflags.io/" + countryCode + "/flat/64.png";

                    chart.tooltip().useHtml(true).format('<img src="' + flagImage + '"/>');
                }
            }
        });

        // initiate drawing the chart
        chart.draw();

    })
});