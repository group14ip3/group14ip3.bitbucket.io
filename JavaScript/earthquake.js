var map;
var location;
var titleName;

var options = {
    zoom: 1,
    minZoon:1,
    center: {
        lat: 0,
        lng: 0
    }
}

// Dynamically Created Buttons.
$(document).ready(function createButtons() {

    var btnPastHour = $('<button/>', {
        text: "Past Hour", //set text 1 to 10
        id: "pastHour",
        class: "eq-btns btn-block btn btn-sm btn-primary",
    });

    var btnPastDay = $('<button/>', {
        text: "Past Day", //set text 1 to 10
        id: "pastDay",
        class: "eq-btns btn-block btn btn-sm btn-primary",
    });

    var btnPastWeek = $('<button/>', {
        text: "Past Week", //set text 1 to 10
        id: "pastWeek",
        class: "eq-btns btn-block btn btn-sm btn-primary",
    });

    var btnPastMonth = $('<button/>', {
        text: "Past Month", //set text 1 to 10
        id: "pastMonth",
        class: "eq-btns btn-block btn btn-sm btn-primary",
    });

    $("#earthquake-buttons").append(btnPastHour);
    $("#earthquake-buttons").append(btnPastDay);
    $("#earthquake-buttons").append(btnPastWeek);
    $("#earthquake-buttons").append(btnPastMonth);

});

/********
 * Initiating Map.
 */
function initMap() {

    map = new google.maps.Map(document.getElementById('gMaps'), options);

}

/********
 * Retrieving the data from API and populating google maps with the data when an event occurs (button click).
 */
$(document).ready(function () {

    initMap();

    /*****
     * Past Hour Visualisation
     */
    $('#pastHour').click(function () {

        initMap();

        var mag = $('#chooseMag').find(":selected").text();
        var apiURL;

        if (mag === "All Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_hour.geojson";
        }

        if (mag === "1.0+ Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/1.0_hour.geojson";
        }

        if (mag === "2.5+ Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_hour.geojson";
        }

        if (mag === "4.5+ Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_hour.geojson";
        }

        if (mag === "Significant Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/significant_hour.geojson";
        }

        $.ajax({

            url: apiURL,

            error: function () {
                alert('An error has occurred');
            },

            success: function (data) {

                console.log(data);

                var markers = [];
                i = 0;

                $.each(data.features, function (key, val) {

                    var locCoordinates = val.geometry.coordinates;
                    
                    console.log(locCoordinates[1]);
                    console.log(locCoordinates[0]);

                    var latLng = new google.maps.LatLng(locCoordinates[1], locCoordinates[0]);

                    if(val.properties.mag === null){
                        val.properties.mag = 0;
                    } // Fixed issue where maps weren't clustering.

                    var marker = new google.maps.Marker({
                        position: latLng,
                        map: map,
                        label: val.properties.mag.toString(),
                    });

                    var queryString = "?" + latLng; 

                    var infowindow = new google.maps.InfoWindow({
                        content: "<p>" + val.properties.title + "</p><a href='" + "earthquake-info-page.htm" + queryString + "' target='" + "_blank" + "'>Details</a>"
                    });

                    marker.addListener('click', function () {
                        infowindow.open(map, marker);
                    });

                    markers[i++] = marker;

                });

                var markerCluster = new MarkerClusterer(map, markers, {
                    imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
                });

            }
        });
    });

    /*****
     * Past Day Visualisation
     */
    $('#pastDay').click(function () {

        initMap();

        var mag = $('#chooseMag').find(":selected").text();
        var apiURL;

        if (mag === "All Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson";
        }

        if (mag === "1.0+ Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/1.0_day.geojson";
        }

        if (mag === "2.5+ Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_day.geojson";
        }

        if (mag === "4.5+ Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_day.geojson";
        }

        if (mag === "Significant Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/significant_day.geojson";
        }

        $.ajax({

            url: apiURL,

            error: function () {
                alert('An error has occurred');
            },

            success: function (data) {

                var markers = [];
                i = 0;

                $.each(data.features, function (key, val) {

                    var locCoordinates = val.geometry.coordinates;

                    var latLng = new google.maps.LatLng(locCoordinates[1], locCoordinates[0]);

                    if(val.properties.mag === null){
                        val.properties.mag = 0;
                    } // Fixed issue where maps weren't clustering.

                    var marker = new google.maps.Marker({
                        position: latLng,
                        map: map,
                        label: val.properties.mag.toString()
                    });

                    var queryString = "?" + latLng; 

                    var infowindow = new google.maps.InfoWindow({
                        content: "<p>" + val.properties.title + "</p><a href='" + "earthquake-info-page.htm" + queryString + "' target='" + "_blank" + "'>Details</a>"
                    });

                    marker.addListener('click', function (data) {
                        infowindow.open(map, marker);
                    });

                    markers[i++] = marker;

                });

                var markerCluster = new MarkerClusterer(map, markers, {
                    imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
                });

            }
        });
    });

    /*****
     * Past Week Visualisation
     */
    $('#pastWeek').click(function () {

        initMap();

        var mag = $('#chooseMag').find(":selected").text();
        var apiURL;

        if (mag === "All Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.geojson";
        }

        if (mag === "1.0+ Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/1.0_week.geojson";
        }

        if (mag === "2.5+ Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_week.geojson";
        }

        if (mag === "4.5+ Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_week.geojson";
        }

        if (mag === "Significant Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/significant_week.geojson";
        }

        $.ajax({

            url: apiURL,

            error: function () {
                alert('An error has occurred');
            },

            success: function (data) {

                var markers = [];
                i = 0;

                $.each(data.features, function (key, val) {

                    var locCoordinates = val.geometry.coordinates;

                    var latLng = new google.maps.LatLng(locCoordinates[1], locCoordinates[0]);

                    if(val.properties.mag === null){
                        val.properties.mag = 0;
                    } // Fixed issue where maps weren't clustering.

                    var marker = new google.maps.Marker({
                        position: latLng,
                        map: map,
                        label: val.properties.mag.toString()
                    });

                    var queryString = "?" + latLng; 

                    var infowindow = new google.maps.InfoWindow({
                        content: "<p>" + val.properties.title + "</p><a href='" + "earthquake-info-page.htm" + queryString + "' target='" + "_blank" + "'>Details</a>"
                    });

                    marker.addListener('click', function (data) {
                        infowindow.open(map, marker);
                    });

                    markers[i++] = marker;

                });

                var markerCluster = new MarkerClusterer(map, markers, {
                    imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
                });

            }
        });
    });

    /*****
     * Past Month Visualisation
     */
    $('#pastMonth').click(function () {

        initMap();

        var mag = $('#chooseMag').find(":selected").text();
        var apiURL;

        if (mag === "All Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_month.geojson";
        }

        if (mag === "1.0+ Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/1.0_month.geojson";
        }

        if (mag === "2.5+ Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_month.geojson";
        }

        if (mag === "4.5+ Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_month.geojson";
        }

        if (mag === "Significant Earthquakes") {
            apiURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/significant_month.geojson";
        }

        $.ajax({

            url: apiURL,

            error: function () {
                alert('An error has occurred');
            },

            success: function (data) {

                var markers = [];
                i = 0;

                $.each(data.features, function (key, val) {

                    var locCoordinates = val.geometry.coordinates;

                    var latLng = new google.maps.LatLng(locCoordinates[1], locCoordinates[0]);

                    if(val.properties.mag === null){
                        val.properties.mag = 0;
                    } // Fixed issue where maps weren't clustering.

                    var marker = new google.maps.Marker({
                        position: latLng,
                        map: map,
                        label: val.properties.mag.toString()
                    });

                    var queryString = "?" + latLng; 

                    var infowindow = new google.maps.InfoWindow({
                        content: "<p>" + val.properties.title + "</p><a href='" + "earthquake-info-page.htm" + queryString + "' target='" + "_blank" + "'>Details</a>"
                    });

                    marker.addListener('click', function (data) {
                        infowindow.open(map, marker);
                    });

                    markers[i++] = marker;

                });

                var markerCluster = new MarkerClusterer(map, markers, {
                    imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
                });

            }
        });
    });
});