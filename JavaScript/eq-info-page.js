// https://www.codeproject.com/Questions/795191/Passing-JavaScript-data-values-between-HTML-pages
// https://css-tricks.com/snippets/javascript/trim-firstlast-characters-in-string/
// https://developer.here.com/projects/PROD-adb0c2c2-3c37-4401-9297-21de271be3f8

$(document).ready(function () {
    var queryString = decodeURIComponent(window.location.search);

    queryString = queryString.substring(2); // Remove ?(
    queryString = queryString.substring(0, queryString.length - 1); // Remove )

    var queries = queryString.split("&"); // Remove &
    var apiURL = "";

    for (var i = 0; i < queries.length; i++) {
        apiURL = apiURL + queries[i];
    }

    var latLonArray = apiURL.split(",");

    var latitude = latLonArray[0];

    var longitude = latLonArray[1];

    longitude = longitude.substring(1); // Remove space

    console.log(latitude);
    console.log(longitude);

    var platform = new H.service.Platform({
        'app_id': 'PUDEJBeSDlP7rUkQjKF5',
        'app_code': 'AKmswq50oW7AU97DmM0Yhg',
        useHTTPS: true
    });

    function setMapLoc(map) {
        map.setCenter({
            lat: latitude,
            lng: longitude
        });
        map.setZoom(10);
    }

    var pixelRatio = window.devicePixelRatio || 1;
    var defaultLayers = platform.createDefaultLayers({
        tileSize: pixelRatio === 1 ? 256 : 512,
        ppi: pixelRatio === 1 ? undefined : 320
    });

    var map = new H.Map(document.getElementById('map'),
        defaultLayers.normal.map, {
            pixelRatio: pixelRatio
        });

    var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
    var ui = H.ui.UI.createDefault(map, defaultLayers, "en-US");

    setMapLoc(map);

    var locMarker = new H.map.Marker({
        lat: latitude,
        lng: longitude
    });

    map.addObject(locMarker);

    console.log(apiURL);

    $.ajax({

        // API Source https://opencagedata.com/api#request
        url: "https://api.opencagedata.com/geocode/v1/json?key=866837e85a3c4b73af2e0ac2713dc854&q=" + apiURL,

        error: function () {

            alert("Error getting information about this location.");

        },

        success: function (data) {

            console.log(data);

            var locationName;
            var currencyName;
            var currencySymbol;
            var currencySubunit;
            var flagCode = data.results[0].components.country_code;

            console.log(flagCode);

            try {
                locationName = data.results[0].formatted;
            } catch (error) {
                locationName = "Location Unavailable";
            }

            try {
                currencyName = $("<li />", {
                    text: "Currency Name: " + data.results[0].annotations.currency.name
                });
            } catch (error) {
                currencyName = $("<li />", {
                    text: "Currency Name Unavailable"
                });
            }

            try {
                currencySubunit = $("<li />", {
                    text: "Currency Subunit: " + data.results[0].annotations.currency.subunit
                });
            } catch (error) {
                currencySubunit = $("<li />", {
                    text: "Currency Subunit Unavailable"
                });
            }

            try {
                currencySymbol = $("<li />", {
                    text: "Currency Symbol: " + data.results[0].annotations.currency.symbol
                });
            } catch (error) {
                currencySymbol = $("<li />", {
                    text: "Currency Symbol Unavailable"
                });
            }

            var flagImage = "https://www.countryflags.io/" + flagCode + "/flat/64.png";

            var countryFlag = $("<img />", {
                src: flagImage,
                alt: "Country Flag"
            });

            $("#loc-name").append(locationName);
            $("#loc-name").append(countryFlag);

            $("#currency-info").append(currencyName);
            $("#currency-info").append(currencySubunit);
            $("#currency-info").append(currencySymbol);



        }
    });

    $.ajax({

        url: "http://api.apixu.com/v1/current.json?key=321d2a6a08de4520909173444193003&q=" + apiURL,

        success: function (data) {

            document.getElementById("currentWeatherImage").src = "http:" + data.current.condition.icon;

            try {
                var lastUpdated = $("<li />", {
                    text: "Last Updated: " + data.current.last_updated
                });
            } catch (error) {
                var lastUpdated = $("<li />", {
                    text: "Last Updated: Unavailable"
                });
            }

            try {
                var weathCond = $("<li />", {
                    text: "Current Condition: " + data.current.condition.text
                });
            } catch (error) {
                var weathCond = $("<li />", {
                    text: "Weather Condition: Unavailable"
                });
            }

            try {
                var tempC = $("<li />", {
                    text: "Current Temparature (℃): " + data.current.temp_c + " ℃"
                });
            } catch (error) {
                var tempC = $("<li />", {
                    text: "Current Temparature (℃): Unavailable"
                });
            }

            try {
                var feelsLikeC = $("<li />", {
                    text: "Feels Like: " + data.current.feelslike_c + " ℃"
                });
            } catch (error) {
                var feelsLikeC = $("<li />", {
                    text: "Feels Like: Unavailable"
                });
            }

            try {
                var tempF = $("<li />", {
                    text: "Current Temparature (℉): " + data.current.temp_f + " ℉"
                });
            } catch (error) {
                var tempF = $("<li />", {
                    text: "Current Temparature (℉): Unavailable"
                });
            }

            try {
                var feelsLikeF = $("<li />", {
                    text: "Feels Like: " + data.current.feelslike_f + " ℉"
                });
            } catch (error) {
                var feelsLikeF = $("<li />", {
                    text: "Feels Like: Unavailable"
                });
            }

            try {
                var windMPH = $("<li />", {
                    text: "Wind (MPH): " + data.current.wind_mph + " mph"
                });
            } catch (error) {
                var windMPH = $("<li />", {
                    text: "Wind (MPH): Unavailable"
                });
            }

            try {
                var gustMPH = $("<li />", {
                    text: "Gusts (MPH): " + data.current.gust_mph + " mph"
                });

            } catch (error) {
                var gustMPH = $("<li />", {
                    text: "Gusts (MPH): Unavailable"
                });

            }

            $("#weather-info").append(lastUpdated);
            $("#weather-info").append(weathCond);
            $("#weather-info").append(tempC);
            $("#weather-info").append(feelsLikeC);
            $("#weather-info").append(tempF);
            $("#weather-info").append(feelsLikeF);
            $("#weather-info").append(windMPH);
            $("#weather-info").append(gustMPH);

        }

    });

});