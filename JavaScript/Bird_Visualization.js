var BirdImage;
$(document).ready(function () {
  displayPlayer(1);
  // create a preloader

  $.ajax({
    url: "https://ebird.org/ws2.0/data/obs/GB/recent/notable?detail=simple&key=kp297rn6u7n5&fmt=json",
    error: function (data) {
      alert('ebird data is not retrived please check Internet Connection and refresh');
    },
    success: function (data) {

      // set chart theme
      anychart.theme('morning');

      var map = anychart.map();
      map.geoData('anychart.maps.united_kingdom').padding(0);

      map.unboundRegions()
        .enabled(true)
        .fill('#E1E1E1') //color fill of the map
        .stroke('#d1d1d1'); // country boundary colors

      map.credits()
        .enabled(true)
        .url('https://ebird.org/region/GB/activity?yr=all&m=')
        .text('Data source: https://ebird.org/explore')
        .logoSrc("/Resources/Images/nav/logo1.png");

      // map.title()
      //   .enabled(true)
      //   .useHtml(true)
      //   .padding([20, 0, 10, 0])
      //   .orientation("top")
      //   .align("center")
      //   .text('<strong>Bird Visualization</strong><br/>' +
      //     '<span style="color:#929292; font-size: 12px;">' +
      //     'Data was filtered to UK birds location</span>');

      $.each(data.slice(0, 1000), function (key, val) {
        // create data set for each bird
        var birDataSet = [{
          lat: val.lat,
          long: val.lng,
          name: val.comName,
          sciName: val.sciName,
          howMany: val.howMany,
          date: val.obsDt,
          location: val.locName,
          bir_Spe_Code: val.speciesCode
        }];
        var rancolor = '#' + Math.floor(Math.random() * 16777215).toString(16);

        var comNameDataSet = anychart.data.set(birDataSet).mapAs();
        // sets marker series and series settings set as true
        var birdLocations = map.marker(birDataSet).enabled(true);

        var createSeries = function (name, birDataSet, rancolor) {


          // create a map as legend
          legend = map.legend();
          // enable the legend
          legend.enabled(true);
          // configure the font of legend items
          legend.fontColor("#455a64");
          legend.fontSize(13);
          legend.fontWeight(10);
          // Title the legend
          legend.title('Legends');
          legend.background("none")
          // Set width.
          legend.width(200);
          // set the source mode of the legend can be as series or categories
          legend.itemsSourceMode("series");
          // set the layout of the legend
          legend.itemsLayout("vertical");
          // set the position mode of the legend inside or outside
          legend.positionMode("outside");
          // set the position of the legend
          legend.position("right");
          //set the hover a the mouse points
          legend.hoverCursor("pointer");
          // set the padding of the legend
          legend.padding(0);
          // set the alignment of the legend
          legend.align("top");
          // enable the drag-and-drop mode of the legend
          legend.drag(true);
          // Get margin.
          var margin = legend.margin();
          margin.top(0);
          margin.right(0);
          margin.bottom(28);
          margin.left(0);


          $('#birdlistButton').on("click", function () {
            var selectedbird = $('#birdlist').find("option:selected").text();

            window.open("https://www.allaboutbirds.org/guide/"+
             selectedbird.replace(/ /g, "_") + "/id");

            console.log(selectedbird.replace(/ /g, "_"));
            console.log(carName);
          });

          birdLocations.name(name)
            .fill(rancolor)
            .stroke('2 #E1E1E1')
            .type('circle')
            .size(4)
            .labels(false)
            .selectionMode('single-select');

          birdLocations.hovered() //if added help will make larger the cursur
            .stroke('2 #fff')
            .size(8);

          birdLocations.legendItem()
            .iconType('circle')
            .iconFill(rancolor)
            .iconStroke('2 #E1E1E1');

        };

        for (var i = 0; i < birDataSet.length; i++) {

          var option = document.createElement("OPTION");
          //Set Customer Name in Text part.
          option.innerHTML = birDataSet[i].name;
          createSeries(option.innerHTML, comNameDataSet.filter('howMany', filter_function(1, 4)), rancolor);
          //Set CustomerId in Value part.
          option.value = birDataSet[i].bir_Spe_Code;
          //Add the Option element to DropDownList.
          birdlist.options.add(option);
        }

        // Enables map tooltip and sets settings for tooltip
        birdLocations.tooltip().title().fontColor('#fff');
        birdLocations.tooltip().titleFormat(function () {
          return this.getData('name')
        });


        birdLocations.tooltip()
          .useHtml(true)
          .padding([8, 13, 10, 13])
          .width(350)
          .fontSize(12)
          .fontColor('#e6e6e6')
          .format(function () {
            var summary = '<span style="color: #bfbfbf">Date seen: ' + '</span>' + this.getData('date');

            if (this.getData('date') == 'null') summary = '';
            return '<table><tr><th><span style="color: #bfbfbf">Scintific name: ' + '</span>"' + this.getData('sciName') + '"<br/>' +
              '<span style="color: #bfbfbf">Place: ' + '</span>' + this.getData('location') + '<br/>' +
              summary + '<br/>' +
              '<span style="color: #bfbfbf">Total amount: ' + '</span>' + this.getData('howMany') + ' bird/s <br/></th>' +
              '<th><span style="color: #bfbfbf"><img src="' + BirdImage + '"<br/></span> </th> </tr></table>'

          });

        birdLocations.listen("pointMouseOver", function (e) {
          var index = e.point.getIndex();
          var bird = birDataSet[index];
          create(bird).then(function (flickrData) {
            console.log(flickrData);
            //if statement for the map to be found
            if (flickrData.items.length === 0) {
              console.log('No photos found for ' + bird.name + ', retrying with different species');
              BirdImage = 'No photos found for ' + bird.name + ', retrying with different species';
            } else {
              console.log(flickrData.items.length + ' images found for ' + bird.name);
              BirdImage = flickrData.items[0].media.m.replace('_m', '_s');
            }
          });
        });

        // var birdlist = document.getElementById("birdsound");

        birdLocations.listenOnce("pointClick", function (e) {
          var index = e.point.getIndex();
          var bird = birDataSet[index];
          getsoundData(bird.sciName).
          then(function (birdSound_Data) {
            if (birdSound_Data.recordings.length === 0) {
              console.log('No sound found for ' + bird.name + ', retrying with different species');
              alert('Selected bird sound NOT found');
            } else {
              console.log('Sound found for ' + birdSound_Data.recordings[0].gen + bird.name);
              displayPlayer(birdSound_Data.recordings[0].id);
            }
          });
        });

        birdLocations.listen("pointDblClick", function (e) {
          var index = e.point.getIndex();
          var bird = birDataSet[index]
          window.open("https://en.wikipedia.org/wiki/" + bird.sciName);
        });

        map.listenOnce("mouseMove", function () {
          var label = map.label(0);
          label.useHtml(true);
          label.text('<span style="text-decoration: underline; font-weight: 900;"><em>Single Click</em></span> a dot to hear the bird sound or <span style="text-decoration: underline; font-weight: 900;"><em>Double Click</em></span> a dot to read on wikipedia.<br/>' + '<span style="text-decoration: underline; font-weight: 900;font-size: 12px;"><em>Click</em></span> anywhere to clear this text');
          label.offsetX(100);
          label.offsetY(300);
          label.fontWeight(600);
          label.background({
            fill: '#9aff9a',
            corners: 3
          });
          label.padding(10)
        });

        map.listenOnce("Click", function () {
          // set label false
          var label = map.label(0, false);

        });

      });
      //preloader with to load the data
      preloader = anychart.ui.preloader();
      preloader.render(document.getElementById("vis-container"));
      preloader.visible(true);

      map.crosshair(true);

      // set the label formatting
      var yLabel = map.crosshair().yLabel();
      var xLabel = map.crosshair().xLabel();

      yLabel.format(function () {
        return "Lat: " + this.value;
      });
      xLabel.format(function () {
        return "Lon: " + this.value;
      });

      var labels = map.labels();
      labels.enabled(true);

      //sets the axis on
      map.axes(true);
      //sets the grids on
      map.grids(true);

      // create zoom controls
      var zoomController = anychart.ui.zoom();
      zoomController.render(map);
      // map size container
      map.container('vis-container');
      map.draw();

      setTimeout(function () {
        // hide preloader after 5 seconds
        preloader.visible(false);
      }, 800)
    }
  });
});

function create(bird) {


  var url = "https://api.flickr.com/services/feeds/photos_public.gne?tags=" + bird.name + "," + bird.sciName + "&tagmode=all&format=json&jsoncallback=?";
  console.log(url);
  return Promise.resolve($.ajax({
    async: false,
    url: url,
    type: "GET",
    dataType: "json",
    error: function () {
      alert("Issue obtaining API data. Please install and enable the free  CROS extension on your browser");
    },
  }));
}

function getsoundData(birdName) {
  var url = "https://jsonp.afeld.me/?url=https://www.xeno-canto.org/api/2/recordings?query=" + birdName.replace(" ", "%20") + "%20q:A&page=1";
  console.log(url);
  return Promise.resolve($.ajax({
    async: false,
    url: url,
    type: "GET",
    dataType: "json",
    error: function () {
      alert("Issue obtaining API data. Please install and enable the free  CORS Toggle extension on your browser.Check on bird tutorial page");
    },
  }));
}

function displayPlayer(songId) {
  $("#player").html($("<iframe/>", {
    src: "https://www.xeno-canto.org/" + songId + "/embed?simple=1",
    scrolling: "no",
    frameborder: "0",
    width: '100%',
    height: '110'
  }));

}

function filter_function(val1, val2) {
  if (val2)
    return function (fieldVal) {
      return val1 <= fieldVal && fieldVal < val2;
    };
  else
    return function (fieldVal) {
      return val1 <= fieldVal;
    };
}