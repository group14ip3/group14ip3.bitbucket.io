var male = [];
var female = [];
var country = [];
var mydata = [];
var birthRate = [];
var iso2countryCode = [];



$.when(

    // ##### GetJSON birthRate
    $.getJSON("https://api.worldbank.org/v2/country/all/indicator/SP.DYN.TFRT.IN?DATE=2016&format=json&per_page=264",

        function (birthData) {
            // Populate array with birth
            for (i = 0; i < birthData[1].length; i++) {
                birthRate.push(birthData[1][i].value);

            }


        }),

    // ##### GetJSON males
    $.getJSON("https://api.worldbank.org/v2/country/all/indicator/SP.DYN.AMRT.MA?DATE=2016&format=json&per_page=264",

        function (maleData) {
            // Populate array with male
            for (i = 0; i < maleData[1].length; i++) {
                male.push(maleData[1][i].value);
                country.push(maleData[1][i].country.value)
            }


        }),


    //##### GetJSON femlaes
    $.getJSON("https://api.worldbank.org/v2/country/all/indicator/SP.DYN.AMRT.FE?DATE=2016&format=json&per_page=264",

        function (femaleData) {
            // Populate array with female
            for (i = 0; i < femaleData[1].length; i++) {
                female.push(femaleData[1][i].value);
            }
        }),

    // Need the iso2 country code for finding flag images
    $.getJSON("https://api.worldbank.org/v2/country/?format=json&per_page=300",
        function (countryData) {
            iso2countryCode = countryData;
        }),


).then(function () {

    for (i = 47; i < male.length; i++) {
        mydata.push([country[i], male[i] * globalVariable.deaths, female[i] * globalVariable.deaths, birthRate[i] * globalVariable.births]);
    }
    //console.log(mydata);

    // Splice out null values
    for (i = 0; i < mydata.length; i++) {
        for (j = 0; j < 3; j++) {

            if (mydata[i][j] === null) {
                mydata.splice(i, 1);
            }
            if (mydata[i][j] === null) {
                mydata.splice(i, 1);
            }
        }

    }
    mydata.splice(6, 1);

    var data = anychart.data.set(mydata);
    var fdata = anychart.data.set(mydata);
    var bdata = anychart.data.set(mydata);

    console.log(mydata);

    chart = anychart.column();

    var series1 = data.mapAs({
        x: 0,
        value: 1
    });
    var series2 = data.mapAs({
        x: 0,
        value: 2
    });
    var series3 = data.mapAs({
        x: 0,
        value: 3
    });

    // create a column series and set the data
    series1 = chart.area(series1);
    series2 = chart.area(series2);
    series3 = chart.line(series3)
    // Name data
    series1.name("Male")
        .color('HotPink');
    series2.name("Female");
    series3.name("Births").color('black');

    //  series1.stroke("2 blue 2");D

    series1.normal().fill("#00B9FF", 1);
    series1.hovered().fill("#00B9FF", 0.5);
    series1.selected().fill("#00B9FF", 1);
    series1.normal().stroke("#00B9FF", 1, "10 5", "round");
    series1.hovered().stroke("#00B9FF", 2, "10 5", "round");
    series1.selected().stroke("#00B9FF", 4, "10 5", "round");


    //  series2.stroke("2 pink 2");
    series2.normal().fill("HotPink", 1);
    series2.hovered().fill("HotPink", 0.9);
    series2.selected().fill("#ffb4d9", 1);
    series2.normal().hatchFill("#ffb4d9", 1, 15);
    series2.hovered().hatchFill("forward-diagonal", "#ffb4d9", 1, 15);
    series2.selected().hatchFill("forward-diagonal", "HotPink", 1, 15);
    series2.normal().stroke("#ffb4d9");
    series2.hovered().stroke("#ffb4d9", 2);
    series2.selected().stroke("#ffb4d9", 4);


    chart.title("Mortality rate of adults (per 1,000) * " + globalVariable.deaths + "\nBirths (per woman) * " + globalVariable.births)

    // set the titles of the axes
    chart.xAxis().title("Countries");
    chart.yAxis().title("Deaths * " + globalVariable.deaths);

    // Rotates x lables and places them inside chart
    chart.xAxis().labels().rotation(-90);
    chart.xAxis().labels().position("outside");
    chart.xAxis().labels().width(100);

    // Display the legend at the top of the chart
    chart.legend(true);
    chart.tooltip().fontColor("gold").format("{%value} of {%seriesName}");
    // set the container id
    chart.container("mortality-graph");

    /* Opens wikipedia page of element double clicked on */
    // add a listener
    chart.listen("pointDblClick", function (e) {
        var index = e.point.getIndex();
        var country = mydata[index]
        window.open("https://en.wikipedia.org/wiki/" + country[0]);
    });

    // initiate drawing the chart
    chart.draw();

});